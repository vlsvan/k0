import java.util.*;

class Answer {

   public static void main (String[] param) {

      // conversion double -> String
      double someDouble = 13.4;
      String someDoubleInString = String.valueOf(someDouble);
      System.out.println(someDoubleInString);

      // conversion String -> int
      String someString = "12374";
      int someInt = Integer.parseInt(someString);
      System.out.println(someInt);

      // "hh:mm:ss"
      Calendar calendar = Calendar.getInstance();
      Date timeDate = calendar.getTime();
      String time = String.format("%02d:%02d:%02d", timeDate.getHours(), timeDate.getMinutes(), timeDate.getSeconds());
      System.out.println(time);

      // cos 45 deg
      double angleDegrees = 45.0;
      double angleCos = Math.cos(Math.toRadians(angleDegrees));
      System.out.println(angleCos);

      // table of square roots
      for (int i = 0; i <= 100; i+=5) {
         System.out.println(i + "\t" + Math.sqrt(i));
      }

      String firstString = "ABcd12";
      String result = reverseCase (firstString);
      System.out.println ("\"" + firstString + "\" -> \"" + result + "\"");

      // reverse string
      String stringToReverse ="1234ab";
      String reversedString = "";
      for (int i = stringToReverse.length() - 1; i > -1; i-=1) {
         reversedString += stringToReverse.charAt(i);
      }
      System.out.println(reversedString);

      String s = "How  many	 words   here";
      int nw = countWords (s);
      System.out.println (s + "\t" + String.valueOf (nw));

      // pause. COMMENT IT OUT BEFORE JUNIT-TESTING!
        /* Calendar beforePause = Calendar.getInstance();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        Calendar afterPause = Calendar.getInstance();
        long timeDifference = afterPause.getTimeInMillis() - beforePause.getTimeInMillis();
        System.out.println("Difference between two moments of time in milliseconds: " + timeDifference);*/

      final int LSIZE = 100;
      ArrayList<Integer> randList = new ArrayList<Integer> (LSIZE);
      Random generaator = new Random();
      for (int i=0; i<LSIZE; i++) {
         randList.add (new Integer (generaator.nextInt(1000)));
      }

      // minimal element
      ArrayList<Integer> randListCopy = new ArrayList<>(randList);
      Collections.sort(randListCopy);
      int minimalElement = randListCopy.get(0);
      System.out.println("Minimal element = " + minimalElement);

      // HashMap tasks:
      //    create
      //    print all keys
      //    remove a key
      //    print all pairs
      HashMap<String, String> subjectsHashMap = new HashMap<>();
      subjectsHashMap.put("ICD0001", "Algoritmid ja andmestruktuurid");
      subjectsHashMap.put("ICD0002", "Arvutigraafika");
      subjectsHashMap.put("ICD0007", "Veebitehnoloogiad");
      subjectsHashMap.put("ICD0008", "Programming in C#");
      subjectsHashMap.put("ITI0102", "Programmeerimise algkursus");
      for (String key : subjectsHashMap.keySet()) {
         System.out.println(key);
      }
      subjectsHashMap.remove("ICD0007");
      for (String key : subjectsHashMap.keySet()) {
         System.out.println(key + " - " + subjectsHashMap.get(key));
      }

      System.out.println ("Before reverse:  " + randList);
      reverseList (randList);
      System.out.println ("After reverse: " + randList);

      System.out.println ("Maximum: " + maximum (randList));
   }

   /** Finding the maximal element.
    * @param a Collection of Comparable elements
    * @return maximal element.
    * @throws NoSuchElementException if <code> a </code> is empty.
    */
   static public <T extends Object & Comparable<? super T>>
   T maximum (Collection<? extends T> a)
           throws NoSuchElementException {
      return a.stream().sorted(Comparator.reverseOrder()).findFirst().get();
   }

   /** Counting the number of words. Any number of any kind of
    * whitespace symbols between words is allowed.
    * @param text text
    * @return number of words in the text
    */
   public static int countWords (String text) {
      String[] test = text.split("\\s+");
      int result = test.length;
      if (test[0].equals("")) {
         result--;
      }
      return result;
   }

   /** Case-reverse. Upper -> lower AND lower -> upper.
    * @param s string
    * @return processed string
    */
   public static String reverseCase (String s) {
      String result = "";
      for (int i = 0; i < s.length(); i++) {
         if (Character.isUpperCase(s.charAt(i))) {
            result += Character.toLowerCase(s.charAt(i));
         } else if (Character.isLowerCase(s.charAt(i))) {
            result += Character.toUpperCase(s.charAt(i));
         } else {
            result += s.charAt(i);
         }
      }
      return result;
   }

   /** List reverse. Do not create a new list.
    * @param list list to reverse
    */
   public static <T extends Object> void reverseList (List<T> list)
           throws UnsupportedOperationException {
      Collections.reverse(list);
   }
}
